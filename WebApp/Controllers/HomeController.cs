﻿//using IotClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {

        /*
        static HttpClient client = new HttpClient();
        static async Task<SmokeDetector> GetSmokeDetectorAsync(string path)
        {
            SmokeDetector SmokeDetector = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                SmokeDetector = await response.Content.ReadAsAsync<SmokeDetector>();
            }
            return SmokeDetector;
        }

        static async Task<SmokeDetector> UpdateSmokeDetectorAsync(SmokeDetector SmokeDetector)
        {
            HttpResponseMessage response = await client.PutAsJsonAsync(
                $"api/SmokeDetectors/{SmokeDetector.Id}", SmokeDetector);
            response.EnsureSuccessStatusCode();

            // Deserialize the updated SmokeDetector from the response body.
            SmokeDetector = await response.Content.ReadAsAsync<SmokeDetector>();
            return SmokeDetector;
        }



        public async Task<ActionResult> ResetAlarm(string alarmName)
        {
            var url = new Uri("http://localhost:49912/api/SmokeDetectors/" + alarmName);

            SmokeDetector SmokeDetector = new SmokeDetector();
            // Get the SmokeDetector
            SmokeDetector = await GetSmokeDetectorAsync(url.PathAndQuery);
            //ShowSmokeDetector(SmokeDetector);

            // Update the SmokeDetector
            Console.WriteLine("Updating Status...");
            SmokeDetector.Status = 0;
            await UpdateSmokeDetectorAsync(SmokeDetector);

            return Index();
        }
        */

        public ActionResult SmokeDetector()
        {
            return View();
        }

        public ActionResult Alarm()
        {
            return View();
        }

        public ActionResult Door()
        {
            return View();
        }


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}