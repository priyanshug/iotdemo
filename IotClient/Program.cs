﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace IotClient
{

    public class SmokeDetector
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public string Location { get; set; }
    }


    class Program
    {
        /// <summary>
        /// Http client responsible to add, delete devices and buzz alarm by sending http request to the API
        /// </summary>
        static HttpClient client = new HttpClient();


        /// <summary>
        /// Request the API and fetch all sensor data
        /// </summary>
        /// <param name="SmokeDetector"></param>
        static void ShowSmokeDetector(SmokeDetector SmokeDetector)
        {
            Console.WriteLine($"Name: {SmokeDetector.Name}\tStatus: " +
                $"{SmokeDetector.Status}\tLocation: {SmokeDetector.Location}");
        }

        /// <summary>
        /// Create the Smoke sensors by sending the http request for the same
        /// </summary>
        /// <param name="SmokeDetector"></param>
        /// <returns></returns>
        static async Task<Uri> CreateSmokeDetectorAsync(SmokeDetector SmokeDetector)
        {
            HttpResponseMessage response = await client.PostAsJsonAsync(
                "api/SmokeDetectors", SmokeDetector);
            response.EnsureSuccessStatusCode();

            // return URI of the created resource.
            return response.Headers.Location;
        }

        /// <summary>
        /// fetch the specific smoke sensor information
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        static async Task<SmokeDetector> GetSmokeDetectorAsync(string path)
        {
            SmokeDetector SmokeDetector = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                SmokeDetector = await response.Content.ReadAsAsync<SmokeDetector>();
            }
            return SmokeDetector;
        }

        /// <summary>
        /// Sends the signal that sensor has detected fire
        /// </summary>
        /// <param name="SmokeDetector"></param>
        /// <returns></returns>
        static async Task<SmokeDetector> UpdateSmokeDetectorAsync(SmokeDetector SmokeDetector)
        {
            HttpResponseMessage response = await client.PutAsJsonAsync(
                $"api/SmokeDetectors/{SmokeDetector.Id}", SmokeDetector);
            response.EnsureSuccessStatusCode();

            // Deserialize the updated SmokeDetector from the response body.
            SmokeDetector = await response.Content.ReadAsAsync<SmokeDetector>();
            return SmokeDetector;
        }

        /// <summary>
        /// Deletes the sensor
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        static async Task<HttpStatusCode> DeleteSmokeDetectorAsync(string id)
        {
            HttpResponseMessage response = await client.DeleteAsync(
                $"api/SmokeDetectors/{id}");
            return response.StatusCode;
        }

        static void Main()
        {
            RunAsync().GetAwaiter().GetResult();
        }

        static async Task RunAsync()
        {
            // Update port # in the following line.
            //client.BaseAddress = new Uri("http://localhost:64195/");
            client.BaseAddress = new Uri("http://localhost:49912/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            Console.WriteLine("Press Enter after Api is online");

            while (true)
            {

                Console.WriteLine("\nWant to Add an alarm? Press 1 \nWant to Buzz an alarm? Press 2 \nWant to Delete an alarm? Press 3");
                //===========================================================================================
                Console.ReadLine();
                Console.WriteLine("\nEnter Choice : ");
                char ch = (char)Console.Read();
                //Console.WriteLine("ch : "+ch);
                int caseSwitch = ch; caseSwitch -= 48;

                //Console.WriteLine("caseSwitch : "+caseSwitch);
                switch (caseSwitch)
                {
                    case 1:
                        Console.WriteLine("Case 1");

                        Console.WriteLine("Create Alarm :");
                        Console.ReadLine();
                        string alarmName = Console.ReadLine();

                        // Create a new SmokeDetector
                        SmokeDetector SmokeDetector = new SmokeDetector
                        {
                            Id = alarmName,
                            Name = alarmName,
                            Status = 0,
                            Location = "PSN"
                        };

                        var url = await CreateSmokeDetectorAsync(SmokeDetector);
                        Console.WriteLine($"Created Alarm Successfully\n");
                        //Console.WriteLine($"Created at {url}");


                        break;
                    case 2:
                        Console.WriteLine("Case 2");
                        Console.WriteLine("Buzz Alarm :");
                        Console.ReadLine();
                        alarmName = Console.ReadLine();

                        url = new Uri("http://localhost:49912/api/SmokeDetectors/" + alarmName);


                        // Get the SmokeDetector
                        SmokeDetector = await GetSmokeDetectorAsync(url.PathAndQuery);
                        ShowSmokeDetector(SmokeDetector);

                        // Update the SmokeDetector
                        Console.WriteLine("Updating Status...");
                        SmokeDetector.Status = 1;
                        await UpdateSmokeDetectorAsync(SmokeDetector);
                        Console.WriteLine($"Updated Alarm Successfully\n");

                        break;

                    case 3:
                        Console.WriteLine("Case 3");
                        Console.WriteLine("Delete Alarm :");
                        Console.ReadLine();
                        alarmName = Console.ReadLine();
                        url = new Uri("http://localhost:49912/api/SmokeDetectors/" + alarmName);


                                         // Get the updated SmokeDetector
                        SmokeDetector = await GetSmokeDetectorAsync(url.PathAndQuery);
                    ShowSmokeDetector(SmokeDetector);

                        
                        Console.WriteLine("#"+alarmName+"#");
                        SmokeDetector.Id = alarmName;

                        // Delete the SmokeDetector
                        var statusCode = await DeleteSmokeDetectorAsync(SmokeDetector.Id);
                        Console.WriteLine($"Deleted Alarm Successfully\n");
                        //Console.WriteLine($"Deleted (HTTP Status = {(int)statusCode})");
                        break;

                    default:
                        Console.WriteLine("Default case");
                        Console.ReadLine();
                        break;
                }

                Console.ReadLine();
            }


        }

            
    }
}
