﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using IotDemo.Models;
using System.Messaging;
using System.Web.Http.Cors;

namespace IotDemo.Controllers
{   
    [EnableCors(origins: "http://localhost:65258", headers: "*", methods: "*")]

    public class SmokeDetectorsController : ApiController
    {
        private SmokeDetectorDbEntities db = new SmokeDetectorDbEntities();    //SmokeDetectors
        private SmokeDetectorDbEntities1 db1 = new SmokeDetectorDbEntities1(); //Alarms
        private SmokeDetectorDbEntities2 db2 = new SmokeDetectorDbEntities2(); //Doors
        
        /// <summary>
        /// Get the list of all Smoke Detectors from the database
        /// </summary>
        /// <returns></returns>
        // GET: api/SmokeDetectors
        public IQueryable<SmokeDetector> GetSmokeDetectors()
        {
            return db.SmokeDetectors;
        }

        /// <summary>
        /// Get the Smoke Detector from the database whose id is passed in argument
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/SmokeDetectors/5
        [ResponseType(typeof(SmokeDetector))]
        public IHttpActionResult GetSmokeDetector(string id)
        {
            SmokeDetector smokeDetector = db.SmokeDetectors.Find(id);
            if (smokeDetector == null)
            {
                return NotFound();
            }
            return Ok(smokeDetector);
        }

        /// <summary>
        /// Update if any smoke detector have detected smoke
        /// </summary>
        /// <param name="id"></param>
        /// <param name="smokeDetector"></param>
        /// <returns></returns>
        // PUT: api/SmokeDetectors/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSmokeDetector(string id, SmokeDetector smokeDetector)
        {
            //####################### ALARM BUZZING ###################
            var alarms = db1.Alarms;
            foreach (var item in alarms)
            {
                item.Status = "BUZZING";
                db1.Entry(item).State = EntityState.Modified;
            }
            db1.SaveChanges();
            //####################### ALARM BUZZING ###################


            //####################### DOOR OPENED ###################
            var doors = db2.Doors;
            foreach (var item in doors)
            {
                item.Status = "OPENED";
                db2.Entry(item).State = EntityState.Modified;
            }
            db2.SaveChanges();
            //####################### DOOR OPENED ###################


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != smokeDetector.Id)
            {
                return BadRequest();
            }

            db.Entry(smokeDetector).State = EntityState.Modified;

            try
            {
                db.SaveChanges();

                //Calling msmq to send alarm message
                using (MessageQueue alarmQ = new MessageQueue())
                {
                    alarmQ.Path = @".\private$\alarmQ";
                    if (!MessageQueue.Exists(alarmQ.Path))
                    {
                        MessageQueue.Create(alarmQ.Path);
                    }
                    Message mymessage = new Message();
                    mymessage.Body = "buzz alarm";
                    alarmQ.Send(mymessage);
                }
               // CreatePrivateQueues();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SmokeDetectorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Add smoke detector to the db
        /// </summary>
        /// <param name="smokeDetector"></param>
        /// <returns></returns>
        // POST: api/SmokeDetectors
        [ResponseType(typeof(SmokeDetector))]
        public IHttpActionResult PostSmokeDetector(SmokeDetector smokeDetector)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SmokeDetectors.Add(smokeDetector);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SmokeDetectorExists(smokeDetector.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = smokeDetector.Id }, smokeDetector);
        }

        /// <summary>
        /// Deletes the Smoke Detector from the db whose id is passed in argument
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/SmokeDetectors/5
        [ResponseType(typeof(SmokeDetector))]
        public IHttpActionResult DeleteSmokeDetector(string id)
        {
            SmokeDetector smokeDetector = db.SmokeDetectors.Find(id);
            if (smokeDetector == null)
            {
                return NotFound();
            }

            db.SmokeDetectors.Remove(smokeDetector);
            db.SaveChanges();

            return Ok(smokeDetector);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SmokeDetectorExists(string id)
        {
            return db.SmokeDetectors.Count(e => e.Id == id) > 0;
        }
    }
}