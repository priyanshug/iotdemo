﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using IotDemo.Models;

namespace IotDemo.Controllers
{
    [EnableCors(origins: "http://localhost:65258", headers: "*", methods: "*")]
    public class DoorsController : ApiController
    {
        private SmokeDetectorDbEntities2 db = new SmokeDetectorDbEntities2();
        private SmokeDetectorDbEntities1 db1 = new SmokeDetectorDbEntities1(); //Alarms

        /// <summary>
        /// Get the list of all doors from the db
        /// </summary>
        /// <returns></returns>
        // GET: api/Doors
        public IQueryable<Door> GetDoors()
        {
            return db.Doors;
        }

        /// <summary>
        /// Get the door from the db whose id is passed as argument
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Doors/5
        [ResponseType(typeof(Door))]
        public IHttpActionResult GetDoor(string id)
        {
            Door door = db.Doors.Find(id);
            if (door == null)
            {
                return NotFound();
            }

            return Ok(door);
        }

        /// <summary>
        /// Update to close all doors
        /// </summary>
        /// <param name="id"></param>
        /// <param name="door"></param>
        /// <returns></returns>
        // PUT: api/Doors/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDoor(string id, Door door)
        {

            if (id == "all")
            {
                var doors = db.Doors;
                foreach (var item in doors)
                {
                    item.Status = "CLOSED";
                    db.Entry(item).State = EntityState.Modified;

                }
                db.SaveChanges();
            }
            else
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (id != door.Id)
                {
                    return BadRequest();
                }

                db.Entry(door).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                    Thread.Sleep(10000);
                    //####################### ALARM BUZZING ###################
                    var alarms = db1.Alarms;
                    foreach (var item in alarms)
                    {
                        item.Status = "BUZZING";
                        db1.Entry(item).State = EntityState.Modified;

                    }
                    db1.SaveChanges();
                    //####################### ALARM BUZZING ###################
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DoorExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Add door to the db
        /// </summary>
        /// <param name="door"></param>
        /// <returns></returns>
        // POST: api/Doors
        [ResponseType(typeof(Door))]
        public IHttpActionResult PostDoor(Door door)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Doors.Add(door);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DoorExists(door.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = door.Id }, door);
        }

        /// <summary>
        /// Deletes the door from the db whose id is passed in argument
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Doors/5
        [ResponseType(typeof(Door))]
        public IHttpActionResult DeleteDoor(string id)
        {
            Door door = db.Doors.Find(id);
            if (door == null)
            {
                return NotFound();
            }

            db.Doors.Remove(door);
            db.SaveChanges();

            return Ok(door);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DoorExists(string id)
        {
            return db.Doors.Count(e => e.Id == id) > 0;
        }
    }
}