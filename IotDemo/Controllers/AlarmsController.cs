﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using IotDemo.Models;

namespace IotDemo.Controllers
{
    [EnableCors(origins: "http://localhost:65258", headers: "*", methods: "*")] 
    public class AlarmsController : ApiController
    {
        private SmokeDetectorDbEntities1 db = new SmokeDetectorDbEntities1();

        /// <summary>
        /// Get the list of all alarms from the db
        /// </summary>
        /// <returns></returns>
        // GET: api/Alarms
        public IQueryable<Alarm> GetAlarms()
        {
            return db.Alarms;
        }

        /// <summary>
        /// Get the alarm from the db whose id is passed as argument
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Alarms/5
        [ResponseType(typeof(Alarm))]
        public IHttpActionResult GetAlarm(string id)
        {
            Alarm alarm = db.Alarms.Find(id);
            if (alarm == null)
            {
                return NotFound();
            }

            return Ok(alarm);
        }

        /// <summary>
        /// Update to buzz all alarms
        /// </summary>
        /// <param name="id"></param>
        /// <param name="alarm"></param>
        /// <returns></returns>
        // PUT: api/Alarms/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAlarm(string id, Alarm alarm)
        {
            var alarms = db.Alarms;
            foreach (var item in alarms)
            {
                item.Status = "STOPPED";
                db.Entry(item).State = EntityState.Modified;
 
            }
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Add alarm to the db
        /// </summary>
        /// <param name="alarm"></param>
        /// <returns></returns>
        // POST: api/Alarms
        [ResponseType(typeof(Alarm))]
        public IHttpActionResult PostAlarm(Alarm alarm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Alarms.Add(alarm);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (AlarmExists(alarm.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = alarm.Id }, alarm);
        }

        /// <summary>
        /// Deletes the Alarm from the db whose id is passed in argument
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Alarms/5
        [ResponseType(typeof(Alarm))]
        public IHttpActionResult DeleteAlarm(string id)
        {
            Alarm alarm = db.Alarms.Find(id);
            if (alarm == null)
            {
                return NotFound();
            }

            db.Alarms.Remove(alarm);
            db.SaveChanges();

            return Ok(alarm);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AlarmExists(string id)
        {
            return db.Alarms.Count(e => e.Id == id) > 0;
        }
    }
}